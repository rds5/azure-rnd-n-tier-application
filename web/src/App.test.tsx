import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Hello World message', () => {
  render(<App />);
  const welcomeMessage = screen.getByText(/Hello World/i);
  expect(welcomeMessage).toBeInTheDocument();
});
