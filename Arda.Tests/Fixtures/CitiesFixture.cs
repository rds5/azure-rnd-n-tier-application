﻿using Arda.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arda.Tests.Fixtures
{
    public static class CitiesFixture
    {
        private static readonly Random rand = new Random();
        public static int RandomCityId()
        {
            return rand.Next(1000) + 1;
        }

        public static City RandomCity()
        {
            int id = rand.Next(1000);

            return new()
            {
                Id = id,
                Name = "Random city " + id
            };
        }

        public static List<City> GetTestCities() =>
            new()
            {
                RandomCity(),
                RandomCity(),
                RandomCity()
            };
    }
}
