﻿using Arda.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Arda.Api.Repositories;
using Arda.Tests.Fixtures;
using Arda.Api.Entities;

namespace Arda.Tests.Systems.Controllers
{
    public class TestCitiesController
    {
        private readonly Mock<IArdaRepository> mockArdaRepository = new();
        private readonly Mock<ILogger<CitiesController>> logger = new();

        private CitiesController ConstructSut()
        {
            return new CitiesController(logger.Object, mockArdaRepository.Object);
        }

        [Fact]
        public async Task GetCitiesAsync_OnSuccess_Returns200()
        {
            // Arrange
            mockArdaRepository
                .Setup(repo => repo.GetCitiesAsync())
                .ReturnsAsync(CitiesFixture.GetTestCities());
            var sut = ConstructSut();

            // Act
            var result = (OkObjectResult)await sut.GetCitiesAsync();

            // Assert
            result.StatusCode.Should().Be(200);
        }

        [Fact]
        public async Task GetCitiesAsync_OnSuccess_InvokesArdaRepositoryExactlyOnce()
        {
            // Arrange
            mockArdaRepository
                .Setup(repo => repo.GetCitiesAsync())
                .ReturnsAsync(CitiesFixture.GetTestCities());
            var sut = ConstructSut();

            // Act
            var result = await sut.GetCitiesAsync();

            // Assert
            mockArdaRepository.Verify(
                repo => repo.GetCitiesAsync(),
                Times.Once());
        }

        [Fact]
        public async Task GetCitiesAsync_OnSuccess_ReturnsListOfCities()
        {
            // Arrange
            mockArdaRepository
                .Setup(repo => repo.GetCitiesAsync())
                .ReturnsAsync(CitiesFixture.GetTestCities());
            var sut = ConstructSut();

            // Act
            var result = (OkObjectResult)await sut.GetCitiesAsync();

            // Assert
            result.Should().BeOfType<OkObjectResult>();
            result.Value.Should().BeOfType<List<City>>();
        }

        [Fact]
        public async Task GetCitiesAsync_OnNoCitiesFound_ReturnsNotFound()
        {
            // Arrange
            mockArdaRepository
                .Setup(repo => repo.GetCitiesAsync())
                .ReturnsAsync(new List<City>());
            var sut = ConstructSut();

            // Act
            var result = await sut.GetCitiesAsync();

            // Assert
            result.Should().BeOfType<NotFoundResult>();
            var objectResult = (NotFoundResult)result;
            objectResult.StatusCode.Should().Be(404);
        }

        [Fact]
        public async Task GetCityAsync_OnSuccess_Returns200()
        {
            // Arrange
            var sut = ConstructSut();

            // Act
            var result = (OkObjectResult)await sut.GetCityAsync(It.IsAny<Int32>());

            // Assert
            result.StatusCode.Should().Be(200);
        }

        [Fact]
        public async Task GetCityAsync_OnIdPassedEqualToOne_ReturnsBordeaux()
        {
            // Arrange
            var sut = ConstructSut();
            int id = 1;

            // Act
            var result = (OkObjectResult)await sut.GetCityAsync(id);

            // Assert
            result.Value.Should().Be("Bordeaux");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(55)]
        [InlineData(17)]
        [InlineData(-33)]
        public async Task GetCityAsync_OnIdPassedNotEqualToOne_ReturnsParis(int id)
        {
            // Arrange
            var sut = ConstructSut();

            // Act
            var result = (OkObjectResult)await sut.GetCityAsync(id);

            // Assert
            result.Value.Should().Be("Paris");
        }
    }
}
