﻿using Arda.Api.Entities;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arda.Tests.Systems.Entities
{
    public class TestCity
    {
        [Fact]
        public async Task GetCityNameById_OnSuccess_ReturnsString()
        {
            // Arrange
            int id = 0;

            // Act
            var result = City.GetCityNameById(id);

            // Assert
            result.Should().BeOfType<string>();
        }

        [Fact]
        public async Task GetCityNameById_OnIdEqualToOne_ReturnsBordeaux()
        {
            // Arrange
            int id = 1;

            // Act
            var result = City.GetCityNameById(id);

            // Assert
            result.Should().Be("Bordeaux");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(55)]
        [InlineData(17)]
        [InlineData(-33)]
        public void GetCityNameById_OnIdNotEqualToOne_ReturnsParis(int id)
        {
            // Act
            var result = City.GetCityNameById(id);

            // Assert
            result.Should().Be("Paris");
        }
    }
}
