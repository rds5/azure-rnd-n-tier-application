﻿namespace Arda.Api.Entities
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static string GetCityNameById(int id)
        {
            switch (id)
            {
                case 1:
                    return "Bordeaux";
                default:
                    return "Paris";
            }
        }
    }
}
