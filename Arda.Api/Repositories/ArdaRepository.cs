﻿using Arda.Api.Configuration;
using Arda.Api.Entities;
using Dapper;
using Microsoft.Extensions.Options;
using System.Data;
using System.Data.SqlClient;

namespace Arda.Api.Repositories
{
    public interface IArdaRepository
    {
        Task<List<City>> GetCitiesAsync();
    }
    public class ArdaRepository : IArdaRepository
    {
        #region PROPS
        private string _database;
        #endregion

        #region INIT
        public ArdaRepository(IOptionsMonitor<ArdaDbOptions> dbOptions)
        {
#if !DEBUG
            _database = dbOptions.CurrentValue.ConnectionString ?? "";
#else
            _database = dbOptions.CurrentValue.ConnectionString ?? "";
#endif
        }
        #endregion

        #region IMPLEMENTATIONS
        // CRICKET AGENT
        public async Task<List<City>> GetCitiesAsync()
        {
            var p = new DynamicParameters();

            string sql = $@"SELECT * FROM dbo.Cities";

            using (IDbConnection cnn = new SqlConnection(_database))
            {
                List<City> cities = await Task.Run(() => cnn.Query<City>(sql, p).ToList());
                return cities;
            }
        }
        #endregion

        #region PRIVATE

        #endregion
    }
}
