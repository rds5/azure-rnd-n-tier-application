﻿namespace Arda.Api.Configuration
{
    public class ArdaDbOptions
    {
        public string ConnectionString { get; set; }
    }
}
