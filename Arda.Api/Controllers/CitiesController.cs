﻿using Arda.Api.Entities;
using Arda.Api.Repositories;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Arda.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        private readonly string[] cities = { "Paris", "Bordeaux", "Lyon", "Strasbourg" };

        private readonly IArdaRepository _ardaRepository;
        private readonly ILogger<CitiesController> _logger;

        public CitiesController(ILogger<CitiesController> logger, IArdaRepository ardaRepository)
        {
            _logger = logger;
            _ardaRepository = ardaRepository;
        }

        // GET api/<CitiesController>
        [HttpGet()]
        public async Task<IActionResult> GetCitiesAsync()
        {
            var cities = await _ardaRepository.GetCitiesAsync();

            if (cities is null || cities.Count == 0) return NotFound();

            // Entity to Dto

            return Ok(cities);
        }

        // GET api/<CitiesController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCityAsync(int id)
        {
            return Ok(City.GetCityNameById(id));
        }
    }
}
