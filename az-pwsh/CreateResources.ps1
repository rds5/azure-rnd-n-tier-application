param([Parameter(Mandatory)] $subscription, 
    [Parameter(Mandatory)] $app = 'arda', 
    [Parameter(Mandatory)] $admin, 
    [Parameter(Mandatory)] $password)

$resourceGroup = "rg-prod-$($app)"
$location = "France Central"
Write-Output "Creating $resourceGroup in $location..."
az group create --name $resourceGroup --location "$location"

$server = "srv-$($app)"
$thisPublicIp = (Invoke-WebRequest -uri "http://ifconfig.me/ip").Content
$ridsIp = "92.103.174.138"
Write-Output "Creating sql server $server..."
az sql server create --name $server --resource-group $resourceGroup --location $location --admin-user $admin --admin-password $password --enable-public-network true
az sql server firewall-rule create --resource-group $resourceGroup --server $server -n AllowAzureIp --start-ip-address "0.0.0.0" --end-ip-address "0.0.0.0"
az sql server firewall-rule create --resource-group $resourceGroup --server $server -n AllowExternalIp --start-ip-address $thisPublicIp --end-ip-address $thisPublicIp
az sql server firewall-rule create --resource-group $resourceGroup --server $server -n AllowRidsIp --start-ip-address $ridsIp --end-ip-address $ridsIp

$db = "db-$($app)"
Write-Output "Creating database $db in server $server..."
az sql db create --resource-group $resourceGroup --server $server --name $db --edition GeneralPurpose --family Gen5 --capacity 2 --zone-redundant true

$connectionString = "Server=tcp:$server.database.windows.net,1433;Database=$db;User ID=$admin;Password=$password;Encrypt=true;Connection Timeout=30;"
Write-Output "Connection available at :"
Write-Output "$connectionString"

./PopulateCities.ps1 -conn $connectionString