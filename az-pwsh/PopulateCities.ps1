param([Parameter(Mandatory)] $conn)

Write-Output "Populating database"

Install-Module sqlserver -Force

Invoke-Sqlcmd -ConnectionString $conn -InputFile "./data.sql"