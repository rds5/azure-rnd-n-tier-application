param([Parameter(Mandatory)] $app = 'arda', 
    [Parameter(Mandatory)] $admin, 
    [Parameter(Mandatory)] $password)

$server = "srv-$app"
$db = "db-$app"

$connectionString = "Server=tcp:$server.database.windows.net,1433;Database=$db;User ID=$admin;Password=$password;Encrypt=true;Connection Timeout=30;"

Write-Output "Setting up connection string in Api env file..."
((Get-Content -path ../Arda.Api/appsettings.json -Raw) -replace '{{connection string}}',$connectionString | Set-Content -path ../Arda.Api/appsettings.json)

Write-Debug "New appsettings.json :"
(Get-Content -path ../Arda.Api/appsettings.json -Raw)