Param(
    [parameter(mandatory)][string]$app
)

$resourceGroup="rg-prod-$($app)"

Write-Output "Cleaning up resource $resourceGroup"
az group delete --name $resourceGroup -y