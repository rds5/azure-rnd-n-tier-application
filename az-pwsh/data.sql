CREATE TABLE DBO.CITIES (
    id      INT NOT NULL IDENTITY,
    [name]  NVARCHAR(255) NOT NULL,

    CONSTRAINT PK_City PRIMARY KEY (id)
);

INSERT INTO DBO.CITIES VALUES
(N'Paris'), (N'Bordeaux'), (N'Lyon'), (N'Strasbourg')